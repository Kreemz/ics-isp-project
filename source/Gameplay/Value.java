package Gameplay;
public class Value
{
  public static int groundGrass = 0;
  public static int groundRoad = 1;
  public static int groundArtery = 2;
  public static int groundTissue = 3;
  
  public static int airAir = -1;
  public static int airFinish = 0;
  public static int airGarbageBin = 1;
  public static int airLaserTower = 2;  
  public static int airGreenWBC = 3;
  public static int airOrangeWBC = 4;
  public static int airPurpleWBC = 5;
  public static int airFinishHeart = 6;
  //public static int airPurpleCell = 8;
  public static int mobAir = -1;
  public static int mob1 = 0;
  
  public static int[] deathReward = {5};
}