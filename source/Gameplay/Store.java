package Gameplay;
import java.awt.*;

public class Store
{
  public Rectangle [] button = new Rectangle [shopSize]; //shop buttons
  public static int shopSize = 8; //8 different things you can buy 
  public static int buttonSize = 52;  
  public static int cellSpace = 2; //space separating between the cells
  public static int awayFromRoom = 29;
  public static int iconSize = 20;
  public static int iconSpace = 6;
  
  public static int itemIn = 4; //how big the item border is going to be
  public static int [] buttonID = {Value.airLaserTower, Value.airGreenWBC, Value.airOrangeWBC, Value.airPurpleWBC, Value.airAir, Value.airAir, Value.airAir, Value.airGarbageBin};
  public static int heldID = -1;
  
  public Rectangle buttonHP, buttonCoins;
  public Rectangle coins;
  
  public boolean holdsItem = false;

  //prices
  public static int [] buttonPrice = {10, 5, 5, 5, 0, 0, 0, 0};
  public static int realID = -1;
  
  public Store ()
  {
    init();
  }
  
  public void click()
  {
      for (int i = 0; i < button.length; i++)
      {
        if (button[i].contains(MainPanel.mousePoint) && buttonID [i] != Value.airAir)
        {
          if (buttonID[i] == Value.airGarbageBin) //delete item
          {
            holdsItem = false;
            //heldID = Value.airAir;            
          }
          else
          {
            heldID = buttonID[i];
            realID = i;
            holdsItem = true;
          }
        }
      }
      if (holdsItem) //the item must be held by the mouse before clicking to settle down or place the tower!
      {
        if (MainPanel.bank >= buttonPrice[realID]) //heldID is the index of the shop item!
        {
          for (int y = 0; y<MainPanel.map.block.length; y++)
          {
            for (int x = 0; x<MainPanel.map.block[0].length; x++)
            {
              if (MainPanel.map.block[y][x].contains(MainPanel.mousePoint))
              {
                if (MainPanel.map.block[y][x].groundID != Value.groundArtery && MainPanel.map.block[y][x].airID == Value.airAir)
                {
                  MainPanel.map.block[y][x].airID = heldID;
                  MainPanel.bank -= buttonPrice[realID];
                }
              }
            }
          }
        }
      }
    
  }
  
  public void init ()
  {
    //creates the cells of the shop
    for (int i = 0; i<button.length; i++)
    {
      button[i] = new Rectangle((MainPanel.myWidth/2) - ((shopSize*(buttonSize+cellSpace))/2) + ((buttonSize+cellSpace)*i), (MainPanel.map.block[MainPanel.map.worldHeight-1][0].y) + MainPanel.map.blockSize + awayFromRoom, buttonSize, buttonSize);
    }
    
    buttonHP = new Rectangle(MainPanel.map.block[0][0].x-1, button[0].y, iconSize, iconSize);
    buttonCoins = new Rectangle(MainPanel.map.block[0][0].x-1, button[0].y + button[0].height-iconSize, iconSize, iconSize);
  }
  
  public void draw(Graphics g)
  {
    for (int i = 0; i<button.length; i++)
    {
      //light up store buttons
      if (button[i].contains(MainPanel.mousePoint)) //if the screen contains the mouse
      {
        g.setColor (new Color(255,255,255,150)); //fourth parameter is transparency
        g.fillRect (button[i].x, button[i].y, button[i].width, button[i].height);
      }
      g.drawImage (MainPanel.tileset_res[0], button[i].x, button[i].y, button[i].width, button[i].height, null);
      if (buttonID[i] != Value.airAir)
      {
        g.drawImage (MainPanel.tileset_air[buttonID[i]], button[i].x + itemIn, button[i].y + itemIn, button[i].width - 2*itemIn, button[i].height - 2*itemIn, null);
      }
      if (buttonPrice [i] > 0)
      {
        g.setColor (Color.WHITE);
        g.setFont (new Font("Courier New", Font.BOLD, 12));
        g.drawString("$" + buttonPrice[i], button[i].x + itemIn, button[i].y + itemIn - 11);
      }
    }
    
    g.drawImage(MainPanel.tileset_res[1],buttonHP.x, buttonHP.y, buttonHP.width, buttonHP.height, null);
    g.drawImage(MainPanel.tileset_res[2],buttonCoins.x, buttonCoins.y, buttonCoins.width, buttonCoins.height, null);
    g.setFont(new Font("Courier New", Font.BOLD, 14));
    g.setColor (Color.BLACK);
    g.drawString ("" + MainPanel.health, buttonHP.x+buttonHP.width+iconSpace, buttonHP.y + buttonHP.height);
    g.drawString ("" + MainPanel.bank, buttonCoins.x+buttonCoins.width+iconSpace, buttonCoins.y + buttonCoins.height);
    
    if (holdsItem)
    {
      g.drawImage(MainPanel.tileset_air[heldID], MainPanel.mousePoint.x - ((button[0].width - 2*itemIn)/2) + itemIn, MainPanel.mousePoint.y - ((button[0].height - 2*itemIn)/2) + itemIn, button[0].width - 2*itemIn, button[0].height - 2*itemIn, null);
    }
  }
}