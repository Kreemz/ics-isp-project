package Gameplay;
import java.awt.*;

public class Mob extends Rectangle
{
  public int mobSize = 52;
  public int mobID = Value.mobAir;
  public boolean inGame = false;
  public int mobWalk = 0; //how long the mob has walked
  
  public final int UPWARD = 0;
  public final int DOWNWARD = 1;
  public final int RIGHT = 2;
  public final int LEFT = 3;
  
  //boolean variables to check if the mob has moved ____ position
  public boolean hasUpward = false;
  public boolean hasDownward = false;
  public boolean hasRight = false;
  public boolean hasLeft = false;
  
  public int direction = RIGHT;
  
  public int xCoord;
  public int yCoord;
  
  public int health;
  public int healthGap = 3;
  
  public int moveFrame = 0;
  public static int mobSpeed = 30;
  int position;
  
  public Mob(int position) 
  {
    this.position = position;
  }
  
  public void spawnMob(int mobID)
  {
    MainPanel.spawnTime = 900 - ((MainPanel.level-1) * 333); //so after one enemy there won't be a huge spawnTime
    //MainPanel.spawnTime = 9001 - ((MainPanel.level-1) * 333); //so after one enemy there won't be a huge spawnTime
    
    for (int y = 0; y < MainPanel.map.block.length; y++)
    {
      if (MainPanel.map.block[y][0].groundID == Value.groundArtery)
      {
        setBounds(MainPanel.map.block[y][0].x, MainPanel.map.block[y][0].y, mobSize, mobSize);
        xCoord = 0;
        yCoord = y;
        if (position == 1)
          break;
      }
    }
    
    //setBounds(10,10,100,100); //test to see if mob spawns    
    this.mobID = mobID;
    this.health = mobSize;
    inGame = true;
    //System.out.println(this.health);
  }
  
  public void deleteMob()
  {
    inGame = false;
    direction = RIGHT; //so the next mob won't move weirdly
    mobWalk = 0;
    if (health == 0)
      MainPanel.map.block[0][0].getMoney(mobID);
  }
  
  public void loseHealth()
  {
    MainPanel.health--;
  }  
  
  // method for movement
  public void process()
  {
    if (moveFrame >= mobSpeed)
    {
      if (direction == RIGHT)
        x++;
      else if (direction == UPWARD)
        y--;
      else if (direction == DOWNWARD)
        y++;
      else 
        x--;
      
      mobWalk++;
      //does another check, _____ coordinate is incremented by one, so it keeps track of the mob's location
      if (mobWalk == MainPanel.map.blockSize) 
      {
        if (direction == RIGHT)
        {
          xCoord++;
          hasRight = true;
        }
        else if (direction == UPWARD)
        {
          yCoord--;
          hasUpward = true;
        }
        else if (direction == DOWNWARD)
        {
          yCoord++;
          hasDownward = true;
        }
        else 
        {
          if (direction == LEFT)
          {
            xCoord--;
            hasLeft = true;
          }
        }
        
        //prints out coordinates to double check location
        //System.out.println(yCoord+ " : " + "[Y], ");
        
        //try and catch for each to make sure the mob moves within the specified path
        
        if (!hasUpward)
        {
          try{
            if (MainPanel.map.block[yCoord+1][xCoord].groundID == Value.groundArtery)
            {
              direction = DOWNWARD;
            }
          } catch (Exception e) {}
        }     
        
        if (!hasDownward)
        {
          try
          {
            if (MainPanel.map.block[yCoord-1][xCoord].groundID == Value.groundArtery)
            {
              direction = UPWARD;
            }
          } catch (Exception e) {}
        } 
        
        if (!hasLeft)
        {
          try
          {
            if (MainPanel.map.block[yCoord][xCoord+1].groundID == Value.groundArtery)
            {
              direction = RIGHT;
            }
          } catch (Exception e) {}
        }
        
        if (!hasRight)
        {
          try
          {
            if (MainPanel.map.block[yCoord][xCoord-1].groundID == Value.groundArtery)
            {
              direction = LEFT;
            }
          } catch (Exception e) {}
        }
        
        //check if the mob is at the finish
        if (MainPanel.map.block[yCoord][xCoord].airID == Value.airFinishHeart)
        {
          deleteMob();
          loseHealth();
        }
        
        //reset
        hasUpward = false;
        hasDownward = false;
        hasRight = false;
        hasLeft = false;
        mobWalk = 0;
      }
      moveFrame = 0;
    }
    else
      moveFrame ++;
  }
  
  public void loseHealth(int amount)
  {
    health -= amount;
    checkDeath();
  }
  
  public void checkDeath()
  {
    if (health == 0)
    {
      MainPanel.killed++;
      System.out.println ("" + MainPanel.killed);
      deleteMob();
    }
  }
  
  public boolean isDead()
  {
    if (inGame)
      return false;
    return true;
  }
  
  public void draw(Graphics g)
  {    
    g.drawImage(MainPanel.tileset_mob[mobID], x, y, width, height, null);
    
    //Health bar
    g.setColor (new Color(233, 14, 15));
    g.fillRect(x, y-2*healthGap, width, healthGap*2);
    
    g.setColor (new Color(50, 180, 50));
    g.fillRect(x, y-2*healthGap, health, healthGap*2);
    
    g.setColor (Color.BLACK);
    g.drawRect(x, y-2*healthGap, health-1, healthGap*2-1);
  }
}