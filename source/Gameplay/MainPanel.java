package Gameplay;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import Gameplay.Map;
import Main.LeukocyticDefense;

public class MainPanel extends JPanel implements Runnable
{
  public Thread thread = new Thread(this);
  public static boolean isFirst = true;
  public static int myWidth, myHeight;
  public static Map map;
  public static LevelScanner save;
  public static Point mousePoint = new Point (0, 0); //the x and y, 1:16:00:00
  public static Store store; //access to the shop
  
  public static int bank = 100;
  public static int health = 10;
  
  public static int killed = 0;
  public static int killsToWin = 0;
  public static int level = 1;
  public static int maxLevel = 3;
  public static boolean isWin = false;
  
  public static int winTime = 3600, winFrame = 0;
  
  public static Image[] tileset_ground = new Image [100];
  public static Image[] tileset_air = new Image [100];
  public static Image[] tileset_res = new Image [100];
  public static Image[] tileset_mob = new Image [100];
  
  public static Mob[] mobs = new Mob [100];
  
  public static boolean isDebug = false; //debugging mode?
  
  public static int spawnTime = 800;
  public int spawnFrame = 0;
  
  public MainPanel (JFrame frame)
  {
    //setBackground (Color.PINK);
    frame.addMouseListener (new KeyControl());
    frame.addMouseMotionListener (new KeyControl());
    thread.start();
  }
  
  public static void hasWon()
  {
    if (killed == killsToWin)
    {
      isWin = true;
      killed = 0;
      //level++; //well do u keep ur money? if so then this has to be commented out
      //bank = 0; //money reset?
    }
  }
  
  public void init()
  {
    map = new Map ();
    save = new LevelScanner ();
    store = new Store ();
    
    bank += ((level-1)*100);
    health = health + (level-1)*10;
    spawnTime = 800;
    Mob.mobSpeed = (int)(Mob.mobSpeed/2);
    
    for (int i = 0; i<tileset_ground.length; i++)
    {
      tileset_ground[i] = new ImageIcon (getClass().getResource("res/tileset_ground.png")).getImage();
      tileset_ground[i] = createImage(new FilteredImageSource(tileset_ground[i].getSource(), new CropImageFilter(0, 26*i, 26, 26))); //second last are width and height
    }
    for (int i = 0; i<tileset_air.length; i++)
    {
      tileset_air[i] = new ImageIcon (getClass().getResource("res/tileset_air.png")).getImage();
      tileset_air[i] = createImage(new FilteredImageSource(tileset_air[i].getSource(), new CropImageFilter(0, 26*i, 26, 26))); //second last are width and height
    }
    
    tileset_res[0] = new ImageIcon (getClass().getResource("res/cell.png")).getImage();
    tileset_res[1] = new ImageIcon (getClass().getResource("/Gameplay/res/heart.png")).getImage();
    tileset_res[2] = new ImageIcon (getClass().getResource("/Gameplay/res/coin.png")).getImage();
    
    tileset_mob[0] = new ImageIcon (getClass().getResource("res/mob1.png")).getImage();
    
    save.loadSave("levels/stage" + level + ".psq");
    
    for (int i = 0; i < mobs.length; i+=2)
    {
      mobs[i] = new Mob(0);
      mobs[i+1] = new Mob(1);
      //mobs[i].spawnMob(0); //this creates the first initial mob so there is no delay for it to come on-screen!
    }
  }
  
  public void paintComponent (Graphics g)
  {
    if (isFirst)
    {
      myWidth = getWidth();
      myHeight = getHeight();
      init();
      isFirst =false;
    }
    g.setColor (new Color(38, 51, 43));   
    g.fillRect (0, 0, getWidth(), getHeight());
    g.setColor (Color.BLACK);
    g.drawLine (map.block[0][0].x-1, 0, map.block[0][0].x-1, map.block[map.worldHeight-1][0].y + map.blockSize); //left line
    g.drawLine (map.block[0][map.worldWidth-1].x + map.blockSize, 0, map.block[0][map.worldWidth-1].x + map.blockSize, map.block[map.worldHeight-1][0].y + map.blockSize); //right line
    g.drawLine (map.block[0][0].x, map.block[map.worldHeight-1][0].y + map.blockSize, map.block[0][map.worldWidth-1].x + map.blockSize, map.block[map.worldHeight-1][0].y + map.blockSize); //bottom line
    
    map.draw(g); //Drawing the room.
    
    for (int i = 0; i < mobs.length; i++)
    {
      if (mobs[i].inGame)
      {
        mobs[i].draw(g);
      }
    }    
    
    store.draw(g); //Draw the store!
    
    if (health < 1)
      lostMessage(g);
    
    if (isWin)
      completeMessage(g);
  }
  
  public void mobSpawner()
  {
    if (spawnFrame >= spawnTime)
    {
      for (int i = 0; i < mobs.length; i+=2)
      {
        if (!mobs[i].inGame)
        {
          mobs[i].spawnMob(Value.mob1);
          mobs[i+1].spawnMob(Value.mob1);
          break;
        }
      }
      spawnFrame = 0;
    }
    else
    {
      spawnFrame++;
    }
  }
  
  public void completeMessage(Graphics g)
  {
    g.setColor (Color.WHITE);
    g.fillRect (0, 0, getWidth(), getHeight());
    g.setColor (Color.BLACK);
    g.setFont (new Font ("Courier New", Font.BOLD, 14));
    if (level == maxLevel)
      g.drawString ("CONGRATULATIONS, YOU WON THE WHOLE GAME!!! You may now exit...", 15, 25);
    else
      g.drawString ("You have completed Stage " + level + ". Bonus coins: " + (level*100) + " Please wait for the next level...", 15, 25);
  }
  
  public void lostMessage(Graphics g)
  {
    g.setColor (new Color (240, 20, 20));
    g.fillRect (0, 0, myWidth, myHeight);
    g.setColor (Color.WHITE);
    g.setFont (new Font ("Courier New", Font.BOLD, 14));
    g.drawString ("GAME OVER, THE PATHOGENS HAVE TAKEN OVER!!!", 10, 20);
  }
  
  public void run ()
  {
    while (true)
    {
      if (!isFirst && health > 0 && !isWin)
      {
        map.process();
        mobSpawner();
        for (int i = 0; i < mobs.length; i++)
        {
          if (mobs[i].inGame)
          {
            mobs[i].process();
          }
        }
      }
      if (isWin)
      {
        if (winFrame >= winTime)
        {
          if (level == maxLevel)
          {
            SwingUtilities.getWindowAncestor(this).dispose();
            new LeukocyticDefense();
          }
          else
          {
            level += 1;
            init();
            isWin = false;              
          }
          winFrame = 0;       
        }
        else
          winFrame++;
      }
      repaint();
      try {Thread.sleep(1);} //1 millisecond and controls loading the map
      catch (Exception e) {}
    }
  }
}