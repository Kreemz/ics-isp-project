package GameState;
import Gameplay.MainPanel;
import javax.swing.*;
import java.awt.*;
public class StageOneState extends GameState
{
  private GameStateManager gsm;
  public StageOneState(GameStateManager gsm)
  {
    this.gsm = gsm;
  }
  
  public void init()
  {  
    SwingUtilities.getWindowAncestor(gsm.pane).setSize(1100, 700);
    SwingUtilities.getWindowAncestor(gsm.pane).setLayout (new GridLayout(1,1,0,0));
    MainPanel mainPanel = new MainPanel((JFrame)SwingUtilities.getWindowAncestor(gsm.pane));
    SwingUtilities.getWindowAncestor(gsm.pane).add (mainPanel);
    SwingUtilities.getWindowAncestor(gsm.pane).setLocationRelativeTo(null);
    SwingUtilities.getWindowAncestor(gsm.pane).remove(gsm.pane);
    gsm.pane = mainPanel;
  }
  
  public void update(){};
  public void draw(java.awt.Graphics2D g){};
  public void keyPressed(int k){};
  public void keyReleased(int k){};
  public void paintComponent(java.awt.Graphics g){};
}