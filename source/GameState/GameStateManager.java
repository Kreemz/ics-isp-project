package GameState;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import Main.GameScreen;

public class GameStateManager
{
  private ArrayList <GameState> gameStates;
  private int currentState;
  JPanel pane;
  SpringLayout layout;
  
  public static final int MENUSTATE = 0;
  
  public GameStateManager(JPanel panez, SpringLayout s) 
  { 
    gameStates = new ArrayList<GameState>();
    currentState = MENUSTATE;
    this.pane = panez;
    this.layout = s;
    gameStates.add(new MenuState(this));
    gameStates.add(new StageOneState(this));
    setState(MENUSTATE);
  }
  
  public void setState(int state) 
  {
    pane.removeAll();
    currentState = state;
    gameStates.get(currentState).init();
  }

  public void update() 
  {
    gameStates.get(currentState).update();
  }
  
  public void draw(java.awt.Graphics2D g) 
  {
    gameStates.get(currentState).draw(g);
  }
  
  public void paintComponent(Graphics g)
  {
    gameStates.get(currentState).paintComponent(g);
  }
  
  public void keyPressed(int k) 
  {
    gameStates.get(currentState).keyPressed(k);
  }
  
  public void keyReleased(int k) 
  {
    gameStates.get(currentState).keyReleased(k);
  }
  
  public void actionPerformed(ActionEvent ae) 
  {
  }
}









