package Main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import GameState.GameStateManager;

/** This class is the content pane of Leukocytic Defense, and contains the main game thread.
  * @author Kareem Golaub
  * @version 2.0 May 23rd, 2014
  */
public class GameScreen extends JPanel implements Runnable//, KeyListener
{
  /** This integer contains the width of the JFrame before scaled.
    */
  static final int WIDTH = 300;
  /** This integer contains the length of the JFrame before scaled.
    */
  static final int LENGTH = 300;
  
  /** This integer is the scale for the width and length of the JFrame.
    */
  static final int SCALE = 2;
  
  /** This integer holds the value of the framerate of the game.
    */
  private int FPS = 40;
  /** This long determines the seconds it takes to get to 1000 frames.
    */
  private long targetTime = 1000/FPS;
  
  //Timer tm = new Timer(60, new TimerListener());
  /** This layout is the layout manager for the JPanel.
    */
  SpringLayout layout = new SpringLayout();
  
  /** This boolean holds the value of whether or not the game thread is running.
    */
  private boolean running;
  
  /** This points to the graphics object of the background, allowing for direct manipulation of its contents.
    */
  Graphics2D g;
  private GameStateManager gsm;
  
  /** This constructor sets running to true, sets the properties of the JPanel and starts the game thread.
    */
  public GameScreen()
  {
    init();
    //tm.start();
    setLayout(layout);
    setPreferredSize(new Dimension(WIDTH*SCALE, LENGTH*SCALE));
    setFocusable(true);
    setBounds(0, 0, 600, 600);
    setBackground(Color.WHITE);
    //addKeyListener(this);
    new Thread(this).start();
  }
  
  /*
   private class TimerListener implements ActionListener 
   {
   public void actionPerformed(java.awt.event.ActionEvent e) 
   {
   setConstraints();
   validate();
   repaint();
   System.out.println(neuDist);
   }
   }
   */
  
  /** Overrides the paintComponent class in JPanel. It calls the superclass' paintComponent and calls the gsm's paintComponent right after.
    * @param g Takes in the graphics object of the component.
    * <p><b>Variable Dictionary: Reference, Type, Purpose</b>
    * <ul>
    * <li>g2d, Graphics2D, contains the graphics of the JPanel.
    * </ul>
    */
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    gsm.paintComponent(g);
  }
  
  public void init()
  {
    running = true;
    gsm = new GameStateManager(this, layout);
  }
  
  public void update()
  {
    gsm.update();
  }
  
  public void draw()
  {
    gsm.draw(g);
  }
  
  /** Is the method containing the game loop and changes the values of the constraints.
    * <p><b>Variable Dictionary: Reference, Type, Purpose</b>
    * <ul>
    * <li>start, long, holds the time at the start of the loop
    * <li>elapsed, long, holds the time passed after setting constraints
    * <li>wait, long, holds the millisecond value of how long the thread should sleep for.
    * <li>e, InterruptedException, is the exception for if the thread is interrupted
    * </ul>
    * <p><b>Loops: Condition, Purpose</b>
    * <ul>
    * <li> while(running), continously updates the screen while the game is running
    * </ul>
    */
  public void run()
  {
    long start;
    long elapsed;
    long wait;
    
    while(running) 
    {
      draw();
      update();
      start = System.nanoTime();
      elapsed = System.nanoTime() - start;
      validate();
      repaint();
      wait = (targetTime - elapsed / 1000000);
      if(wait < 0) 
        wait = 25;
      try
      {
        Thread.sleep(wait);
      }
      catch(InterruptedException e) 
      {
        e.printStackTrace();
      }
    }
  }
  
  /** Contains what to do when an action event fires off during the running of the program.
    * @param ae Takes in the action event that occured during the program
    */
  public void actionPerformed(ActionEvent ae)
  { 
  }
  
  /*
   public void keyReleased(KeyEvent e)
   {
   }
   
   public void keyPressed(KeyEvent e)
   {
   }
   
   public void keyTyped(KeyEvent e)
   {
   }
   */
}