package Main;
import javax.swing.*;
import Main.GameScreen;
import Gameplay.FrameApp;
/** This class is the main driver of Leukocytic Defense. It creates the window and starts the game thread.
  * @author Kareem Golaub
  * @version 2.0 May 23rd, 2014
  */
public class LeukocyticDefense
{
  /** This constructor creates the JFrame, with the game title, sets window properties 
    * and adds a GameScreen object to the frame. 
    * <p><b>Variable Dictionary: Reference, Type, Purpose</b>
    * <ul>
    * <li>window, JFrame, is the frame that holds the GameScreen panel
    * </ul>
    */
  public LeukocyticDefense()
  {
    JFrame window = new JFrame("Leukocytic Defense");
    window.add(new GameScreen());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setResizable(false);
    window.setVisible(true);
    window.pack();
    window.setLocationRelativeTo(null);
    window.setFocusable(true);
    window.requestFocus();
  }
  
  /** This method is the main method of the class, and creates a LeukocyticDefense object.
    * @param args Takes in the command-line arguments.
    */
  public static void main (String[]args)
  {
    new LeukocyticDefense();
    //new Frame();
  }
}
