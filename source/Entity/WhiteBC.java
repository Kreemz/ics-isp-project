package Entity;

/** This is the white blood cell class that will contain common attributes of white blood cells in the game.
  * @author Kareem Golaub
  * @version 1.0 May 23rd, 2014
  */
public abstract class WhiteBC extends MapObject
{
}