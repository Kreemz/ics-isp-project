package Entity;

/** This is the pathogen class which will contain common attributes of pathogens in the game.
  * @author Kareem Golaub
  * @version 1.0 May 23rd, 2014
  */
public abstract class Pathogen extends MapObject
{
}