package Entity;
import java.awt.Rectangle;

/** This class contains the common attributes of all objects on the screen.
  */
public abstract class MapObject
{
  /** Horizontal position of object.
    */
  protected double x;
  /** Vertical position of object.
    */
  protected double y;
  /** Loaded width of entity.
    */
  protected int width;
  /** Loaded height of entity.
    */
  protected int height;
  /** Effective collision width of image.
    */
  protected int cwidth;
  /** Effective collision height of image.
    */
  protected int cheight;
  /** Value of current row of collision.
    */
  protected int currRow;
  /** Value of current column of collision.
    */
  protected int currCol;
  /** Horizontal value of destination in event of collision.
    */
  protected double xdest;
  /** Vertical value of destination in event of collision.
    */
  protected double ydest;
  /** Temporary horizontal position.
    */
  protected double xtemp;
  /** Temporary vertical position.
    */
  protected double ytemp;
  /** Value of top left corner of collision box.
    */
  protected boolean topLeft;
  /** Value of top right corner of collision box.
    */
  protected boolean topRight;
  /** Value of bottom left corner of collision box.
    */
  protected boolean bottomLeft;
  /** Value of bottom right corner of collision box.
    */
  protected boolean bottomRight;
  
  // protected Animation animation;
  /** Value of current action.
   */
  protected int currentAction;
  /** Value of the previous action.
   */
  protected int previousAction;
  
  /** Value of whether or not the object is moving left.
   */
  protected boolean left;
  /** Value of whether or not the object is moving right.
   */
  protected boolean right;
  /** Value of whether or not the object is moving up.
   */
  protected boolean up;
  /** Value of whether or not the object is moving down.
   */
  protected boolean down;
  
  /** Value of the object's move speed.
   */
  protected double moveSpeed;
  /** Value of the object's maximum speed.
   */
  protected double maxSpeed;
  /** Value of the speed at which the object decays at after stopping.
   */
  protected double stopSpeed;
  
  /** This method returns whether or not a map object intersects another map object.
    * @param o Contains the other map object being checked for intersection.
   */
  public boolean intersects(MapObject o)
  {
    Rectangle r1 = getRectangle();
    Rectangle r2 = o.getRectangle();
    return r1.intersects(r2);
  }
  
  public Rectangle getRectangle()
  {
    return new Rectangle((int)x - cwidth, (int)y - cheight, cwidth, cheight);
  }
}