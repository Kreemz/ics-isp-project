import java.awt.*; //use graphics

public class Map
{
  public int worldWidth = 12;
  public int worldHeight = 8;
  public int blockSize = 52;
  
  public Block [][] block;
  
  public Map ()
  {
    define();
  }
  
  public void define()
  {
    block = new Block[worldHeight][worldWidth];
    for (int y = 0; y < block.length; y++)
    {
      for (int x = 0; x < block [0].length; x++)
      {
        block[y][x]= new Block((MainPanel.myWidth/2) - (worldWidth*blockSize/2) + x*blockSize, y*blockSize, blockSize, blockSize, Value.groundGrass, Value.airAir);
      }      
    }
  }
  
  public void draw (Graphics g)
  {
    for (int y = 0; y < block.length; y++)
    {
      for (int x = 0; x < block [0].length; x++)
      {
        block[y][x].draw(g);
      }      
    }
    
    //fully shows the range
    for (int y = 0; y < block.length; y++)
    {
      for (int x = 0; x < block [0].length; x++)
      {
        block[y][x].fight(g);
      }      
    }
  }
  
  public void process()
  {
    for (int y=0; y<block.length; y++)
    {
      for (int x=0; x<block[0].length; x++)
      {
        block[y][x].process();
      }
    }
  }
}