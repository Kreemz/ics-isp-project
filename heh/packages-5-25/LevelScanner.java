import java.io.*;
import java.util.*; // for scanner

public class LevelScanner
{
  public void loadSave(File loadPath)
  {
    try
    {
      Scanner sc = new Scanner (loadPath);
      
      while (sc.hasNext())
      {
        MainPanel.killsToWin = sc.nextInt();
        
        for (int y = 0; y<MainPanel.map.block.length; y++)
        {
          for (int x = 0; x<MainPanel.map.block[0].length; x++)
          {
            MainPanel.map.block[y][x].groundID = sc.nextInt();
          }
        }
        
        for (int y = 0; y<MainPanel.map.block.length; y++)
        {
          for (int x = 0; x<MainPanel.map.block[0].length; x++)
          {
            MainPanel.map.block[y][x].airID = sc.nextInt();
          }
        }
      }
      sc.close();
    }
    catch (Exception e)
    {
    }
  }
}