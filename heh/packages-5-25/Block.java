import java.awt.*;

public class Block extends Rectangle
{
  public Rectangle towerSquare; //range
  public int towerRange = 175;
  public int groundID;
  public int airID;
  public int shotMob = -1;
  public boolean shooting = false;
  public int loseTime = 70, loseFrame = 0;
  
  public Block (int x, int y, int width, int height, int groundId, int airId)
  {
    setBounds (x, y, width, height);
    towerSquare = new Rectangle (x - (towerRange/2), y - (towerRange/2), width + towerRange, height + towerRange);
    this.groundID = groundID; //grass block or sand block etc
    this.airID = airID;
  }  
  
  public void draw(Graphics g)
  {
    //g.drawRect(x, y, width, height);
    g.drawImage(MainPanel.tileset_ground[groundID], x, y, width, height, null);
    
    if(airID != Value.airAir)
    {
      g.drawImage(MainPanel.tileset_air[airID], x, y, width, height, null);
      //draw images
    }
  }
  
  public void fight (Graphics g)
  {
    if(MainPanel.isDebug)
    {
      if(airID == Value.airLaserTower)
      {
        g.drawRect(towerSquare.x, towerSquare.y, towerSquare.width, towerSquare.height);    
      }
    }
    if(shooting)
    {
      g.setColor (new Color (255, 255, 0));
      g.drawLine(x+width/2, y+height/2, MainPanel.mobs[shotMob].x + MainPanel.mobs[shotMob].width/2, MainPanel.mobs[shotMob].y + MainPanel.mobs[shotMob].height/2);
    }    
  }
  
  public void process ()
  {        
    if (shotMob != -1 && towerSquare.intersects(MainPanel.mobs[shotMob]))
    {
      shooting = true;
    }
    else
    {
      shooting = false;
    }    
    if(!shooting)
    {
      //each time mob passes it will check all of the mobs in the rectangle and shoot 1 of them
      if(airID == Value.airLaserTower /*|| OR whatever is the name of your tower*/)
      {    
        //check through mob array
        for (int i = 0; i<MainPanel.mobs.length; i++)
        {
          if (MainPanel.mobs[i].inGame)
          {
            if (towerSquare.intersects(MainPanel.mobs[i]))
            {
              shooting = true;
              shotMob = i;
            }
          }
        }      
      }
    }
    if(shooting)
    {
      if(loseFrame>=loseTime)
      {
        MainPanel.mobs[shotMob].loseHealth(1);
        loseFrame = 0;
      }
      else
      {
        loseFrame++;
      }
      
      if(MainPanel.mobs[shotMob].isDead())
      {
        shooting = false;
        shotMob = -1;
        //Screen.killed++;
        MainPanel.hasWon();
      }
    }
  }
  
  public void getMoney(int mobID)
  {
    MainPanel.bank += Value.deathReward[mobID];    
  }
}