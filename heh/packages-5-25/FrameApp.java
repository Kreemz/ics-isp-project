import javax.swing.*;
import java.awt.*;

public class FrameApp extends JFrame
{
  public static String title = "Leukocytic Defense";
  public static Dimension size = new Dimension (700, 550);
  
  public FrameApp()
  {
    setTitle (title);
    setSize (size);
    setResizable (false);
    setLocationRelativeTo (null);
    setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    
    init();
  }
  
  public void init()
  {
    setLayout (new GridLayout(1,1,0,0));
    
    MainPanel mainPanel = new MainPanel(this);
    add (mainPanel);
    
    setVisible (true);
  }
  
  public static void main (String args[] )
  {
    FrameApp frame = new FrameApp();
  }
}